//go:generate go-bindata test.glade

package main

import (
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/gtk"
	"log"
	// "github.com/gotk3/gotk3/glib"
	// "reflect"
	// "runtime"
)

type GUI struct {
	MainWindow         *gtk.Window
	AccelGroups        map[string]*gtk.AccelGroup
	MenuBar            *gtk.MenuBar
	MenuItemFullscreen *gtk.CheckMenuItem
	MenuItemQuit       *gtk.MenuItem
	FullscreenStatus   bool
}

func (gui *GUI) SetFullscreen(desiredStatus bool) {
	log.Print("Setting FS as: ", desiredStatus)
	if gui.FullscreenStatus && !desiredStatus {
		gui.MenuBar.Show()
		gui.MainWindow.Unfullscreen()
		gui.FullscreenStatus = false
		gui.MainWindow.AddAccelGroup(gui.AccelGroups["normal"])
		gui.MainWindow.RemoveAccelGroup(gui.AccelGroups["fullscreen"])
	}

	if !gui.FullscreenStatus && desiredStatus {
		gui.MenuBar.Hide()
		gui.MainWindow.Fullscreen()
		gui.FullscreenStatus = true
		gui.MainWindow.AddAccelGroup(gui.AccelGroups["fullscreen"])
		gui.MainWindow.RemoveAccelGroup(gui.AccelGroups["normal"])
	}

	gui.MenuItemFullscreen.SetActive(gui.FullscreenStatus)
}

func (gui *GUI) ToggleFullscreen() {
	log.Print("Toggling FS, current: ", gui.FullscreenStatus)
	gui.SetFullscreen(!gui.FullscreenStatus)
	log.Print("Toggled FS, current: ", gui.FullscreenStatus)
}

func AccelGroupsConnect(
	key uint,
	mods gdk.ModifierType,
	flags gtk.AccelFlags,
	callback func(),
	accelGroups map[string]*gtk.AccelGroup,
	menuItem interface {
		AddAccelerator(
			string,
			*gtk.AccelGroup,
			uint,
			gdk.ModifierType,
			gtk.AccelFlags)
	},
	signal string) {

	if accelGroups == nil {
		log.Fatal("no accel groups")
	}

	accelGroups["fullscreen"].Connect(key, mods, flags, callback)
	menuItem.AddAccelerator(signal, accelGroups["normal"], key, mods, flags)
}

func main() {
	gtk.Init(nil)

	builder, err := gtk.BuilderNew()
	if err != nil {
		log.Fatal(err)
	}
	test_glade, err := Asset("test.glade")
	if err != nil {
		log.Fatal(err)
	}
	builder.AddFromString(string(test_glade))

	winObj, err := builder.GetObject("MainWindow")
	if err != nil {
		log.Fatal(err)
	}
	mainWin, ok := winObj.(*gtk.Window)
	if !ok {
		log.Fatal(ok)
	}

	menuObj, err := builder.GetObject("MenuBar")
	if err != nil {
		log.Fatal(err)
	}
	menuBar, ok := menuObj.(*gtk.MenuBar)
	if !ok {
		log.Fatal(ok)
	}

	menuItemFullscreenObj, err := builder.GetObject("MenuFullscreen")
	if err != nil {
		log.Fatal(err)
	}
	menuItemFullscreen, ok := menuItemFullscreenObj.(*gtk.CheckMenuItem)
	if !ok {
		log.Fatal(ok)
	}

	menuItemQuitObj, err := builder.GetObject("MenuFileQuit")
	if err != nil {
		log.Fatal("test ", err)
	}
	menuItemQuit, ok := menuItemQuitObj.(*gtk.MenuItem)
	if !ok {
		log.Fatal("test ", ok)
	}

	accelGroupNormal, err := gtk.AccelGroupNew()
	if err != nil {
		log.Fatal(err)
	}

	accelGroupFullscreen, err := gtk.AccelGroupNew()
	if err != nil {
		log.Fatal(err)
	}

	accelGroups := map[string]*gtk.AccelGroup{
		"normal":     accelGroupNormal,
		"fullscreen": accelGroupFullscreen,
	}

	gui := GUI{mainWin, accelGroups, menuBar, menuItemFullscreen, menuItemQuit, false}

	gui.MainWindow.SetTitle("TEST")
	gui.MainWindow.SetDefaultSize(600, 480)
	gui.MainWindow.Connect("destroy", func() { gtk.MainQuit() })

	gui.MenuItemQuit.Connect("activate", func() {
		log.Print("MenuItemQuit activated")
		gtk.MainQuit()
	})

	AccelGroupsConnect(
		gdk.KEY_q,
		gdk.GDK_CONTROL_MASK,
		gtk.ACCEL_VISIBLE,
		func() { gtk.MainQuit() },
		gui.AccelGroups,
		gui.MenuItemQuit,
		"activate")

	gui.MenuItemFullscreen.Connect("toggled", func() {
		log.Print("MenuItemFullscreen toggled")
		gui.SetFullscreen(gui.MenuItemFullscreen.GetActive())
	})

	AccelGroupsConnect(
		gdk.KEY_f,
		gdk.GDK_CONTROL_MASK,
		gtk.ACCEL_VISIBLE,
		func() {
			log.Print("MainWindow Fullscreen Accel called")
			gui.MenuItemFullscreen.SetActive(!gui.FullscreenStatus)
		},
		gui.AccelGroups,
		gui.MenuItemFullscreen,
		"activate",
	)

	gui.MainWindow.AddAccelGroup(gui.AccelGroups["normal"])

	mainWin.ShowAll()
	gtk.Main()
}
